package com.db.rest;

import com.db.grad.database.QueryType;
import com.db.grad.database.TableExtractor;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tables")
public class Service {

    @GET
    @Path("/average")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAverage(){
        String result = TableExtractor.getJson(QueryType.AVG_QUERY);
        return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    @GET
    @Path("/sum")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getCount(){
        String result = TableExtractor.getJson(QueryType.SUM_QUERY);
        return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    /*@GET
    @Path("/instrument")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstrument(){
        String result = TableExtractor.getJson("instrument");
        return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    @GET
    @Path("/deal")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getDeal(){
        String result = TableExtractor.getJson("deal");
        return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    @GET
    @Path("/counterparty")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getCounterparty(){
        String result = TableExtractor.getJson("counterparty");
        return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }*/
}
