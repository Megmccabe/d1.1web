<jsp:useBean id="verifyObject" class="com.db.grad.verification.Verify" scope="request"/>
<%
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	if (verifyObject.performVerification(username, password)) {
	    response.getWriter().print(verifyObject.getJson());
		//response.sendRedirect("success.jsp");
	} else {
        response.getWriter().print(verifyObject.getJson());
		//response.sendRedirect("failure.jsp");
	}
%>