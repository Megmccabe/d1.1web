<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%--<%@include file="validate.jsp"%>--%>

<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- We link the CDN css files here -->
		<!-- Latest compiled and minified CSS-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!--This is my CSS file -->
		<link rel="stylesheet" type="text/css" href="css/main.css">

        <title>index.jsp</title>
    </head>
    <body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Login</h2>
				<!-- form-inline: inline form -->
				<form action="login.jsp" method="post" class="form-horizontal">
					<div class="form-group">
						<label for="username" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10">
							<input type="text" id="username" name="username"/>
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" id="password" name="password"/>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-10 col-sm-push-2">
							<input type="submit" class="btn btn-default" id="submit" value="submit"/>
						</div>
					</div>
				</form>
				<br>
				<%
					if (request.getParameter("error") != null && request.getParameter("error").equals("true")) {
					    out.println("<p>The password you entered was incorrect.</p>");
					}
				%>
			</div>
		</div>
	</div>

	<!-- JavaScript files should be linked at the bottom of the page -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- d3js scripts -->
	<script src="https://d3js.org/d3.v5.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>