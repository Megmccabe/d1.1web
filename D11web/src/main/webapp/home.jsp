<%--
  Created by IntelliJ IDEA.
  User: Graduate
  Date: 10/08/2018
  Time: 12:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <style>
        a {
            background-color: #555555;
            text-align: center;
            color: #ffffff;
            padding: 8px;
            margin: 2px;
            display: block;
            text-decoration: none;
            width: 15%;
        }
    </style>
</head>
<body>
<h1>Home</h1>
<a href="rest/links.jsp?method=1">anonymous_users</a><br>
<a href="rest/links.jsp?method=2">counterparty</a><br>
<a href="rest/links.jsp?method=3">deal</a><br>
<a href="rest/links.jsp?method=4">instrument</a><br>
<a href="rest/links.jsp?method=5">login_trail</a><br>
<a href="rest/links.jsp?method=6">users</a><br>
<a href="jquery.jsp?method=8">jquery</a>
</body>
</html>
