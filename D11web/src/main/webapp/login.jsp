<jsp:useBean id="verifyObject" class="com.db.grad.verification.Verify" scope="request"/>
<%
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    if (verifyObject.performVerification(username, password)) {
        //response.getWriter().print(verifyObject.getJson());
        response.sendRedirect("home.jsp");
    } else {
        //response.getWriter().print(verifyObject.getJson());
        response.sendRedirect("index.jsp?error=true");
    }
%>